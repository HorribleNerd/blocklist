/*
 * MIT License
 *
 * Copyright (c) 2020 HorribleNerd
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package com.horriblenerd.blocklist;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLLoadCompleteEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Mod(modid = Blocklist.MODID, name = Blocklist.NAME, version = Blocklist.VERSION)
@Mod.EventBusSubscriber(modid = Blocklist.MODID)
public class Blocklist {
    public static final String MODID = "blocklist";
    public static final String NAME = "Blocklist";
    public static final String VERSION = "1.0";

    private static Logger logger;

    private static List<Block> handBlockList;
    private static List<Block> pickBlockList;
    private static List<Block> axeBlockList;
    private static List<Block> shovelBlockList;
    private static List<Block> swordBlockList;
    private static List<Block> hoeBlockList;
    private static List<Block> shearBlockList;
    private static List<Block> blockPlaceList;

    @SubscribeEvent
    public static void onBlockBreak(BlockEvent.BreakEvent event) {
        Block block = event.getWorld().getBlockState(event.getPos()).getBlock();
        if (block == Blocks.AIR || event.getPlayer().isCreative())
            return;
        ItemStack stack = event.getPlayer().getHeldItemMainhand();
        Item tool = stack.getItem();
        boolean contains;
        if (tool instanceof ItemPickaxe)
            contains = pickBlockList.contains(block);
        else if (tool instanceof ItemAxe)
            contains = axeBlockList.contains(block);
        else if (tool instanceof ItemSpade)
            contains = shovelBlockList.contains(block);
        else if (tool instanceof ItemSword)
            contains = swordBlockList.contains(block);
        else if (tool instanceof ItemHoe)
            contains = hoeBlockList.contains(block);
        else if (tool instanceof ItemShears)
            contains = shearBlockList.contains(block);
        else
            contains = handBlockList.contains(block);

        event.setCanceled(contains != Config.breakingWhitelistMode);

    }

    @SubscribeEvent
    public static void onBlockPlace(BlockEvent.EntityPlaceEvent event) {
        IBlockState blockState = event.getPlacedBlock();
        Block block = blockState.getBlock();
        if (event.getEntity() instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.getEntity();
            if (player.isCreative())
                return;
            boolean contains = blockPlaceList.contains(block);
            event.setCanceled(contains != Config.placingWhitelistMode);
        }
    }

    private static boolean listContainsBlock(String list, Block block, boolean whitelist) {
        for (String s : list.replace(" ", "").replace("\"", "").split(",")) {
            if (Objects.requireNonNull(block.getRegistryName()).toString().equalsIgnoreCase(s)) {
                return whitelist;
            }
        }
        return !whitelist;
    }

    private static List<Block> parseList(String list) {
        ArrayList<Block> ret = new ArrayList<>();
        for (String s : list.replace(" ", "").replace("\"", "").split(",")) {
            ret.add(ForgeRegistries.BLOCKS.getValue(new ResourceLocation(s)));
        }
        return ret;
    }

    private void initLists() {
        handBlockList = parseList(Config.handList);
        pickBlockList = parseList(Config.pickList);
        axeBlockList = parseList(Config.axeList);
        shovelBlockList = parseList(Config.shovelList);
        hoeBlockList = parseList(Config.hoeList);
        shearBlockList = parseList(Config.shearList);
        swordBlockList = parseList(Config.swordList);
        blockPlaceList = parseList(Config.placeList);
    }

    @EventHandler
    public void postInit(FMLLoadCompleteEvent event) {
        initLists();
    }

    @SubscribeEvent
    public void postConfig(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (event.getModID().equals(MODID))
            initLists();
    }

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
    }

    @net.minecraftforge.common.config.Config(modid = Blocklist.MODID, name = Blocklist.NAME, type = net.minecraftforge.common.config.Config.Type.INSTANCE)
    public static class Config {
        @net.minecraftforge.common.config.Config.Comment("Use whitelist instead of blacklist for block breaking")
        public static boolean breakingWhitelistMode = true;
        @net.minecraftforge.common.config.Config.Comment("General (Hand) Breaking Blocklist")
        public static String handList = "";
        @net.minecraftforge.common.config.Config.Comment("Pickaxe Breaking Blocklist")
        public static String pickList = "minecraft:stone";
        @net.minecraftforge.common.config.Config.Comment("Axe Breaking Blocklist")
        public static String axeList = "minecraft:log";
        @net.minecraftforge.common.config.Config.Comment("Shovel Breaking Blocklist")
        public static String shovelList = "minecraft:grass,minecraft:dirt";
        @net.minecraftforge.common.config.Config.Comment("Hoe Breaking Blocklist")
        public static String hoeList = "minecraft:carrots";
        @net.minecraftforge.common.config.Config.Comment("Shears Breaking Blocklist")
        public static String shearList = "minecraft:leaves";
        @net.minecraftforge.common.config.Config.Comment("Sword Breaking Blocklist")
        public static String swordList = "";

        @net.minecraftforge.common.config.Config.Comment("Use whitelist instead of blacklist for block placing")
        public static boolean placingWhitelistMode = true;
        @net.minecraftforge.common.config.Config.Comment("Block Placing Whitelist")
        public static String placeList = "minecraft:stone,minecraft:grass";

    }

}
